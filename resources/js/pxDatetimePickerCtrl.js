(function() {
	'use strict';
	
	/**
	* Controller to manage the DateTime Picker widget.
	* link: https://www.predix-ui.com/?show=px-datetime-picker&type=component
	*/
	function PxDatetimePickerCtrl($scope, $element){
		
		var ctrl = this,
			datetimePicker = $('px-datetime-picker', $element),
			target = $scope.DECISYON.target,
			ctx = target.content.ctx,
			baseName = ctx.baseName.value,
			refObjId = ctx.instanceReference.value,
			mshPage = target.page,
			dateParamToExport = baseName + '_Date',
			dateEpocParamToExport = baseName + '_DateEpoch',
			DEFAULT_TIMEZONE = '',
			DEFAULT_TIMEFORMAT = 'HH:mm A',
			DEFAULT_DATEFORMAT = 'MM/DD/YYYY',
			EMPTY_PARAM_VALUE = 'PARAM_VALUE_NOT_FOUND',
			FORMAT_DATE_PARAM = 'YYYYMMDD',
			FORMAT_UNIX_TIMESTAMP = 'x';
			
		/* Managing of the custom settings from context */
		var manageCustomSettingsFromContext = function(newContext) {
			ctrl.hasFutureDatesBlocked = newContext.$pxblockFutureDates.value;
			ctrl.hasPastDatesBlocked = newContext.$pxblockPastDates.value;
			ctrl.hasTimeHidden = newContext.$pxhideTime.value;
			ctrl.showTimeZone = newContext.$pxshowTimeZone.value.value;
			ctrl.timeZone = (newContext.$pxtimeZone.value.value !== 'AUTO') ? newContext.$pxtimeZone.value.value : DEFAULT_TIMEZONE;
			setAttributesToWidget(newContext);
		};
		
		/* Set specific properties to Predix component */
		var setAttributesToWidget = function(newContext) {
			setBooleanAttributeOnlyIsTrue('block-future-dates', ctrl.hasFutureDatesBlocked);
			setBooleanAttributeOnlyIsTrue('block-past-dates', ctrl.hasPastDatesBlocked);
			setBooleanAttributeOnlyIsTrue('hide-time', ctrl.hasTimeHidden);
			setBooleanAttributeOnlyIsTrue('show-time-zone', ctrl.showTimeZone);
			manageTimeFormat(newContext);
			manageDateFormat(newContext);
			manageTimeZone(newContext);
		};
		
		/* Set the boolean attribute only if true because Predix component doesn't want the attribute in its DOM node if false*/
		var setBooleanAttributeOnlyIsTrue = function(attribute, boolValue) {
			if (boolValue){
				datetimePicker.attr(attribute, boolValue);
			}
		};
		
		/* Managing of date format property */
		var manageDateFormat = function(newContext) {
			var dateFormat = (newContext.$pxdateFormat.value !== '') ? newContext.$pxdateFormat.value : DEFAULT_DATEFORMAT,
				isDateFormatValid = isDateFormatCorrect(dateFormat, ctrl.timeZone),
				validDateFormat = isDateFormatValid ? dateFormat : DEFAULT_DATEFORMAT;
			datetimePicker.attr('date-format', validDateFormat);
			if (!isDateFormatValid){
				console.log('Datetime Picker: invalid date format ' + dateFormat + '. It will be use the default date format: ' + DEFAULT_DATEFORMAT);
			}
		};
		
		/* Check if date format is valid by using today's day to prevent XSS attacks */
		var isDateFormatCorrect = function(dateFormat) {
			var typedInMoment = Px.moment(Px.moment().toISOString()).format(dateFormat),
				isValidDate = Px.moment(typedInMoment,dateFormat, true).isValid();
			return isValidDate;
		};
		
		/* Check if time format is valid by using today's day to prevent XSS attacks */
		var isTimeFormatCorrect = function(timeFormat, timeZone) {
			var typedInMoment = Px.moment.tz(Px.moment().toISOString(), timeFormat, timeZone),
				invalidAt = typedInMoment.invalidAt();
			return (invalidAt !== -1 || !typedInMoment.isValid() ||!typedInMoment._isValid) ? false: true;
		};
		
		/* Managing of time format property */
		var manageTimeFormat = function(newContext) {
			var timeFormat = (newContext.$pxtimeFormat.value !== '') ? newContext.$pxtimeFormat.value : DEFAULT_TIMEFORMAT,
				isTimeFormatValid = isTimeFormatCorrect(timeFormat, ctrl.timeZone),
				validTimeFormat = isTimeFormatValid ? timeFormat : DEFAULT_TIMEFORMAT;
			datetimePicker.attr('time-format', validTimeFormat);
			if (!isTimeFormatValid){
				console.log('Datetime Picker: invalid time format ' + timeFormat + '. It will be use the default time format: ' + DEFAULT_TIMEFORMAT);
			}
		};
		
		/* Managing of time zone property */
		var manageTimeZone = function(newContext) {
			datetimePicker.attr('time-zone', ctrl.timeZone);
		};
		
		/* Function to set the parameters on the page. */
		var sendParams = function(startDate, startDateEpoch) {
			if (mshPage){
				target.sendParamChanged(dateParamToExport, startDate);
				target.sendParamChanged(dateEpocParamToExport, startDateEpoch);
			}
		};
		
		/* Action to fire on change datetime */
		var actionOnChangeDatetime = function(selectedStartDateISOString) {
			var startDate = Px.moment.utc(selectedStartDateISOString).format(FORMAT_DATE_PARAM),
				startDateEpoch = Px.moment.utc(selectedStartDateISOString).format(FORMAT_UNIX_TIMESTAMP);
			sendParams(startDate, startDateEpoch);
		};
		
		/* Set event listener when the input value changes.
		The event px-datetime-submitted can't be used because it creates an infinite loop that is always triggered whenever the value is set (after send param)
		*/
		var setEventOnChangeInput = function() {
			datetimePicker[0].addEventListener('px-cell-blured', function(e) {
				if (!this._opened && this.$.field.isValid){
					actionOnChangeDatetime(this.$.calendar.fromMoment.toISOString());
				}
			});
		};
		
		/* Set event listener when the calendar cell is clicked.
		The event px-datetime-submitted can't be used because it creates an infinite loop that is always triggered whenever the value is set (after send param)
		*/
		var setEventOnClickCalendar = function(calendarCell, target) {
			datetimePicker[0].addEventListener('px-cell-date-selected', function(e) {
				actionOnChangeDatetime(this.$.calendar.fromMoment.toISOString());
			});
		};
		
		/* Set event listeners to widget elements */
		var setEventHandler = function() {
			setEventOnChangeInput();
			setEventOnClickCalendar();
		};
		
		/* Set the date-time setting to Predix component */
		var setDatetimeProperty = function(startDate) {
			datetimePicker.attr('date-time', startDate);
		};
		
		/* Check imported parameters: if defined, if empty */
		var getImportedParamFromContext = function(importedParam) {
			return (angular.isDefined(importedParam) && !angular.equals(importedParam.value, EMPTY_PARAM_VALUE)) ? importedParam.value : '';
		};
		
		/* Return the date in input in YYYYMMDD format */
		var getDateInYYYYMMDD = function(pDate) {
			return parseInt(Px.moment(pDate).format(FORMAT_DATE_PARAM));
		};
		
		/* Return the valide date */
		var getValidDate = function(isDateToChange, originalDate, validDateToSet) {
			if (isDateToChange){
				return validDateToSet;
			}
			return originalDate;
		};
		
		/* Return the date epoch in ISO format string to use to export parameter */
		var getDateEpochToImport = function(dateEpoch) {
			return Px.moment(dateEpoch, FORMAT_UNIX_TIMESTAMP, true).toISOString();
		};
		
		/* Check of imported dates, both params for start-date and params for end-date
		- if paramDate and paramDateEpoch are invalid, use today's date;
		- if paramDateEpoch is valid and attribute hide-time is false, use paramDateEpoch;
		- if paramDateEpoch is invalid and paramDate is valid and hide-time is true, use paramDate
		*/
		var getValidDateToImport = function(pDate, pDateEpoch) {
			var isValidDate = Px.moment(pDate,FORMAT_DATE_PARAM, true).isValid(),
				isValidDateEpoch = parseInt(pDateEpoch) && Px.moment(pDateEpoch,FORMAT_UNIX_TIMESTAMP, true).isValid(),
				dateToYYYYMMDD = (isValidDate) ? parseInt(getDateInYYYYMMDD(pDate)) : '',
				dateEpochToYYYYMMDD = (isValidDateEpoch) ? parseInt(getDateInYYYYMMDD(Px.moment(pDateEpoch,FORMAT_UNIX_TIMESTAMP, true).toISOString())) : '',
				isSameDate = (dateToYYYYMMDD === dateEpochToYYYYMMDD),
				useDateEpoch = isValidDateEpoch && (!ctrl.hasTimeHidden || isSameDate),
				useDate = isValidDate && (ctrl.hasTimeHidden || !isValidDateEpoch);
			return (useDateEpoch) ? getDateEpochToImport(pDateEpoch) : (useDate ? getDateToImport(pDate) : Px.moment().toISOString());
		};
		
		/* Managing of both the param start-date.
		- if both the past dates and the future dates are blocked, use today's date
		- if the future dates are blocked and the imported date is greater than today, use today's date
		- if the past dates are blocked aand the imported date is less than today, use today's date
		*/
		var manageImportedDates = function(startDate) {
			var todayDate = Px.moment().toISOString(),
				todayDateInYYYYMMDD = getDateInYYYYMMDD(todayDate),
				isStartDateGreaterThanToday = getDateInYYYYMMDD(startDate) > todayDateInYYYYMMDD,
				isStartDateLessThanToday = getDateInYYYYMMDD(startDate) < todayDateInYYYYMMDD,
				validStartDate = startDate;
				
			if (ctrl.hasPastDatesBlocked && ctrl.hasFutureDatesBlocked){
				validStartDate = todayDate;
			}else {
				if (ctrl.hasPastDatesBlocked && !ctrl.hasFutureDatesBlocked){
					validStartDate = getValidDate(isStartDateLessThanToday, startDate, todayDate);
				}else if (!ctrl.hasPastDatesBlocked && ctrl.hasFutureDatesBlocked){
					validStartDate = getValidDate(isStartDateGreaterThanToday, startDate, todayDate);
				}
			}
			setDatetimeProperty(validStartDate);
		};
		
		/* Return the date in ISO format string to use to export parameter */
		var getDateToImport = function(date) {
			return Px.moment(Px.moment(date, FORMAT_DATE_PARAM).utc().format(FORMAT_UNIX_TIMESTAMP), FORMAT_UNIX_TIMESTAMP).toISOString();
		};
		
		/* Managing of date parameters to import  */
		var manageImportedParams = function(context) {
			var SUFFIX_IMPORTED_PARAMS = '_PARAM_',
				startDateToImport = getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + dateParamToExport]),
				startDateEpochToImport = getImportedParamFromContext(context[SUFFIX_IMPORTED_PARAMS + dateEpocParamToExport]),
				startDateValidToImport = getValidDateToImport(startDateToImport, startDateEpochToImport);
			manageImportedDates(startDateValidToImport);
		};
		
		/* Watch on widget context to observe parameter changing after loading */
		var setListenerOnDataChange = function() {
			$scope.$watch('DECISYON.target.content.ctx', function(newValue, oldValue) {
				if (!angular.equals(newValue, oldValue)) {
					manageCustomSettingsFromContext(newValue);
					manageImportedParams(newValue);
				}
			},true);
		};
		
		/* Initialize */
		var inizialize = function() {
			ctrl.widgetID = 'pxDatetimePicker_' + refObjId;
			setListenerOnDataChange();
		};
		
		/* Function will be fired once the polymer component are loaded. */
		ctrl.whenPredixDatetimePickerLibraryLoaded = function() {
			DEFAULT_TIMEZONE = Px.moment.tz(Px.moment.tz.guess())._z.name;
			manageCustomSettingsFromContext(ctx);
			manageImportedParams(ctx);
			setEventHandler();
		};
		
		inizialize();
	}
	
	PxDatetimePickerCtrl.$inject = ['$scope', '$element'];
	
	DECISYON.ng.register.controller('pxDatetimePickerCtrl', PxDatetimePickerCtrl);

}());
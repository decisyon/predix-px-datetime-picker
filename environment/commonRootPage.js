/**
 * Function to open user profile page
 * @param owner
 * @param subjectID
 * @returns
 */
function userProfile(owner, subjectID, scope) {
	
	var url = "../misc/userProfile.jsp?userId="+subjectID+"&edit=0";
	var idDialog = subjectID+'_profileDialog';
	
	var width = 1024;
	var height = '80%';
	
	switch (scope) {
	case 'INVITATION':
		customTitle = LANGUAGES.getOriginalLabel("UserProfile");
		jQDialogIframe(idDialog, customTitle, url, width, height, "", true, true, true, true);
		break;

	case 'ONDIALOG' :
		closeAllTips();
		customTitle = LANGUAGES.getOriginalLabel("UserProfile");
		jQDialogIframe(idDialog, customTitle, url, width, height, "", true, true, true, true);
		break;
		
		
	case 'DEFAULT' :
		rootPage.navigateToContent(url);
		break;
		
	default:
		rootPage.navigateToContent(url);
		break;
	}
	
	return;
}

/* ******************************************************************************************
 * Decisyon Api : a singletone object
 ****************************************************************************************** */

var DecisyonApi = (function () {
	var instance;

	function createInstance() {

		var object = {
				userInfo : {},
				localization : {},
				utils : new DcyGenericApi(),
				window : {
					section : {
						element : function(customJsId){
							var dshElement = window.DSH_PAGE.getDshElementFromCustomJsId(customJsId);
							if(dshElement){
								return dshElement.api();
							}
							return null;
						}
					},
					dashboards : function(customJsId){
						return getDshPagesByCustomJsId(customJsId);
					},
					element : function(){}
				},
				ng: {
					register: {}
				},
				// Api only alpha version
				alpha: {
					addWatchToParam: {}
				},
				//Sets
				setSection : function(section){
					//Da ricontrollare. Genera problemi di memory leak perchè referenzia funzioni javascript legate alla window del mashboard
					//instance.window.section = jQuery.extend({}, instance.window.section, section);
				},
				i18n : {},
				l10n : {},
				setUserInfo : function(userInfo){
					if(isValidObject(userInfo)){
						instance.userInfo.selectedLanguage = LANGUAGES.getLanguage().toLowerCase();
						instance.userInfo.name = userInfo.name;
						instance.userInfo.surname = userInfo.surname;
						instance.userInfo.fullname = userInfo.fullname;
					}
				},
				app: {
					logout: function() {
						rootPage.logoutFunction();
					}
				},
				api : function(){
					return {
//						window : object.window,
						i18n : object.i18n,
						ng: {
							register: object.ng.register
						},
						alpha : {
							addWatchToParam : object.alpha.addWatchToParam
						},
						userInfo : {
							selectedLanguage : object.userInfo.selectedLanguage,
							name: object.userInfo.name,
							surname: object.userInfo.surname,
							fullname: object.userInfo.fullname
						},
						app: {
							logout: object.app.logout
						}
					};
				}
		};

		return object;
	}

	return {
		getInstance: function () {
			if (!instance) {
				instance = createInstance();
			}
			return instance;
		}
	};
})();

/*
 *  Decisyon : a singletone object
 */
var Decisyon = (function () {
  var instance;

  function createApiInstance() {
      return DecisyonApi.getInstance();
  }

  return {
      getInstance: function () {
          if (!instance) {
          	instance = createApiInstance();
          }
          return instance;
      }
  };
})();

var DECISYON_CORE = Decisyon.getInstance();


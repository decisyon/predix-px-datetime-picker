//wrapper del filtro angular-translate
(function() {
	'use strict';

	function DcyTranslate($translate, $filter, $rootScope, dcyUtilsFactory, dcyTranslateProvider) {
		 var cached = {};
		 function dcyTranslateFilter(labelId, interpolateParams, language) {
			 language = dcyUtilsFactory.isValidString(language) ? language : dcyTranslateProvider.translateProvider.preferredLanguage();
			 var dcyLabel = '_' + labelId;
			 var idCached = language + dcyLabel;
			 if (idCached in cached) {
				 if(!angular.isFunction(cached[idCached].then)){
					 var result = cached[idCached];
					 $rootScope.$applyAsync(function(){
						 delete cached[idCached];
					 });
					 return result;
				 }
				 else{
					 return undefined;
				 }
			 } else {
				 if(!cached[idCached]){
					 cached[idCached] = $translate(dcyLabel, interpolateParams, null, labelId, language).then(function(translated){
						 cached[idCached] = translated;
					 });
				 }
			 }
		 }

		 dcyTranslateFilter.$stateful = true;

		 return dcyTranslateFilter;
	};

	DcyTranslate.$inject = ['$translate', '$filter', '$rootScope', 'dcyUtilsFactory', 'dcyTranslateProvider'];
	angular.module('dcyApp.filters').filter('dcyTranslate', DcyTranslate);
}());

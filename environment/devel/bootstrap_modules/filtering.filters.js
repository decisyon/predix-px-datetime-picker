/**
 * #### Decisyon Srl ####
 * Author 	: Pagano Francesco
 * Date 	: 20.08.2015 
 */

(function() {
	'use strict';

	var filtersModule = angular.module('dcyApp.filters');
	/**
	 * Si occupa di normalizzare gli url in base al component $sce (sicurezza)
	 */
	function TrustUrl($sce) {
		return function(url) {
			return $sce.trustAsResourceUrl(url);
		};
	};
	
	TrustUrl.$inject = ['$sce'];
	
	filtersModule.filter('trustUrl', TrustUrl);
	/**
	 * Si occupa di tradurre le label in base al classico sistema DCY
	 */
	function Translator() {
		return function(labelId, aliasCollection) {
			// Set default value for aliasCollection
			aliasCollection = typeof aliasCollection !== 'undefined' ? aliasCollection : null;
			
			var text = getLabel(labelId);
			
			if(aliasCollection) {
				text = window.replaceAllForPosition(text, '%(\\d+)', aliasCollection);
			}
			
			return text;
		};
	};
	
	Translator.$inject = [];
	
	filtersModule.filter('translator', Translator);
	/**
	 * Si occupa di tradurre le label in base al classico sistema DCY
	 */
	function IsValidObject(dcyUtilsFactory) {
		return function(value) {
			return dcyUtilsFactory.isValidObject(value);
		};
	};
	
	IsValidObject.$inject = ['dcyUtilsFactory'];
	
	filtersModule.filter('isValidObject', IsValidObject);

}());
